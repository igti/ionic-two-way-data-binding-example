import { Component } from '@angular/core';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  /**
   * Guarda o tamanho do texto digitado
   */
  tamanhoTexto = 0;

  /**
   * Texto digitado
   */
  textoComTwoWayDataBinding = 'Com two-way data binding';
  textoSemTwoWayDataBinding = 'Sem two-way data binding';

  constructor() {}

  processar(event) {
    this.textoSemTwoWayDataBinding = event.value;
  }
}
